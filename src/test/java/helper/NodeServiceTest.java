package helper;

import model.Node;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class NodeServiceTest {

    private final NodeService service = new NodeService();
    private List<Node> nodes;

    @Before
    public void setUp() {
        nodes = IntStream.range(1, 8)
                .boxed()
                .map(Node::new)
                .collect(Collectors.toList());

        for (int i = 1; i < nodes.size(); i++) {
            nodes.get(i - 1).setNext(nodes.get(i));
        }

        nodes.get(nodes.size() - 1).setNext(nodes.get(0));
    }

    @Test
    public void detectAndRemoveLoopTest() {
        final boolean actual = service.detectAndRemoveLoop(nodes.get(0));
        Assert.assertEquals(true, actual);
    }

}
