package helper;

import model.Node;

import java.util.Objects;

public class NodeService {


    public boolean detectAndRemoveLoop(final Node firstNode) {
        Node slow = firstNode;
        Node fast = firstNode;

        while (Objects.nonNull(slow) && Objects.nonNull(fast) && Objects.nonNull(fast.getNext())) {
            slow = slow.getNext();
            fast = fast.getNext().getNext();
            if (slow == fast) {
                removeLoop(slow, firstNode);
                return true;
            }
        }
        return false;
    }

    private void removeLoop(final Node cycle, final Node currentNode) {
        Node first = currentNode;

        Node second;
        while (true) {
            second = cycle;
            while (second.getNext() != cycle && second.getNext() != first) {
                second = second.getNext();
            }
            if (second.getNext() == first) {
                break;
            }
            first = first.getNext();
        }

        second.setNext(null);

    }
}
