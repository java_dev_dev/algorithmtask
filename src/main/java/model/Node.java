package model;

import lombok.Data;

@Data
public class Node {

    private final int data;
    private Node next;

}


